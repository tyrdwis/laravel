<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('register');
    }
    public function welcome(Request $req){
        // dd($req->all());
        $namaDepan = $req["namaDepan"];
        $namaBelakang = $req["namaAkhir"];

        echo "<h1> Selamat Datang $namaDepan $namaBelakang! <h1>";
        echo "<h2>Terimakasih telah bergabung di SanberBook. Social Media kita bersama!</h2>";
    }

}
