<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
    @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="namaDepan"> <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="namaAkhir"> <br><br>
        <label>Gender: </label> <br><br>
        <input type="radio" name="Gender"> Male <br>
        <input type="radio" name="Gender"> Female <br>
        <input type="radio" name="Gender"> Other <br><br>
        <label>Nationality</label> <br><br>
        <select name="negara">
            <option value="Indonesia"> Indonesia </option>
            <option value="Jepang"> Malaysia </option>
            <option value="Korea"> Singapura </option>            
            <option value="Indonesia"> Thailand </option>
            <option value="Jepang"> Jepang </option>
            <option value="Korea"> Korea </option>
        </select> <br><br>
        <label>Language Spoken: </label> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>
        <label>Bio:</label> <br>
        <textarea name="bio"cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up"><br><br>
    </form>
</body>
</html>